import React from 'react';

export default class Computador extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      investimento: 10,
      meses: 5,
      valorTotal: 50
    };

  }

  handleChangeInvestimento = (event) => {
    const newInvestimento = parseFloat(event.target.value);

    this.setState({
      investimento: newInvestimento,
      meses: this.state.valorTotal / newInvestimento
    }); 
  }
  
  handleChangeMeses = (event) => {
    const newMeses = parseFloat(event.target.value);

    this.setState({
      meses: newMeses,
      valorTotal: newMeses * this.state.investimento 
    }); 
  }
  
  handleChangeValor = (event) => {
    const newValor = parseFloat(event.target.value);

    this.setState({
      valorTotal: newValor,
      investimento: newValor / this.state.meses
    });
  }

  render(){
    return(
      <div>
        <p>
          Investimento:
          <input type="number" step="0.01" placeholder="Investimento" id="investimento" value={this.state.investimento} onChange={this.handleChangeInvestimento}></input>
        </p>
        <p>
          Meses:
          <input type="number" placeholder="Meses" id="meses" value={this.state.meses} onChange={this.handleChangeMeses}></input>
        </p>
        <p>
          Valor Total:
          <input type="number" step="0.01" placeholder="Valor Total" id="valorTotal" value={this.state.valorTotal} onChange={this.handleChangeValor}></input>
        </p>
      </div>
    );
  }
}
