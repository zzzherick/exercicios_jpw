function validarPersonagem(){
    const form = document.forms["criacao"];

    const nome = form["nome"].value;
    if(!nome || nome.length < 3){
        throw new Error("O nome do personagem deve conter pelo menos 3 letras!");
    }
    
    if(nome.startsWith("Admin") || nome.startsWith("GM") || nome.startsWith("Moderador")){
        throw new Error("O nome do personagem não pode iniciar com a palavra informada!");
    }

    const cidade = form["cidade"].value;
    const raca = form["raca"].value;

    if(cidade === "Venore" && raca === "Elfo"){
        throw new Error("Elfos são proibidos em Venore!");
    }

    if(cidade === "Carlin" && raca === "Anão"){
        throw new Error("Anões são proibidos em Carlin!");
    }
}

function validarForm() {
    try{
        validarPersonagem();
    }catch(e){
        alert(e.message);
        return false;
    }

    return true;
  } 