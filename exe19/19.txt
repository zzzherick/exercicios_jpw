a) git init - Inicia um repositório git na pasta
b) git config --global user.name "turing" - Configura o nome do usuário do git como "turing" de forma global, para todos os repositórios
c) git add EXERCICIO.txt - Adiciona o arquivo "EXERCICIO.txt" ao index do repositório
d) git add . - Adiciona todos os arquivos ao index do repositório
e) git commit -m "Adicionado nova interface" - Confirma as mudanças feitas com a descrição "Adicionado nova interface", realizando um commit
f) git commit - Confirma as mudanças feitas, porém abre o editor de texto padrão para que seja informada a mensagem do commit
g) git reset --hard HEAD - Remove todas as mudanças que não foram confirmadas
h) cd Downloads - Entra no diretório "Downloads"
i) pwd - Mostra o caminho do diretório atual
j) cd .. - Retorna ao diretório um nível acima
k) ls - Lista todos os itens dentro de um diretório
l) git pull - Incorpora as mundaças feitas no repositório remoto ao repositório local
m) git push - Envia as mudanças locais ao repositório remoto
n) git clone https://gitlab.com/rVenson/meurepositorio - Clona um repositório remoto para um diretório local
   

