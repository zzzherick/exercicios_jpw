const express = require("express");
const app = express();
const fs = require('fs');
const cors = require("cors");

app.use(cors());
app.use(express.json());

app.get("/computadores", (req, res) => {
    res.json(JSON.parse(fs.readFileSync("computadores.json", "utf8")));
});

app.post("/computadores", (req, res) => {
    let computadores = JSON.parse(fs.readFileSync("computadores.json", "utf8"));
    computadores.push(req.body);

    fs.writeFileSync("computadores.json", JSON.stringify(computadores), { encoding: "utf8", flag: "w"});
    res.send();
})

app.delete("/computadores/:index", (req, res) => {

    let computadores = JSON.parse(fs.readFileSync("computadores.json", "utf8"));
    computadores.splice(req.params.index, 1);

    fs.writeFileSync("computadores.json", JSON.stringify(computadores), { encoding: "utf8", flag: "w"});
    
    res.send();
});

app.listen(3001, () => {
    console.log('Aplicação executando na porta 3001');
});