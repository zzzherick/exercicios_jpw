import React from 'react';

export default class ComputadorForm extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      hostname: "",
      processador: "",
      memoria: "",
      armazenamento: "",
      estado: ""
    };
  }

  handleSubmit = (event) => {
    event.preventDefault();

    this.props.handleSubmit({
      hostname: this.state.hostname,
      processador: this.state.processador,
      memoria: this.state.memoria,
      armazenamento: this.state.armazenamento,
      estado: this.state.estado
    });
  }

  handleChange = (event) => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  render(){
    return(
    <form onSubmit={this.handleSubmit}>
      <input type="text" placeholder="Hostname" id="hostname" value={this.state.hostname} onChange={this.handleChange}></input>
      <input type="text" placeholder="Processador" id="processador" value={this.state.processador} onChange={this.handleChange}></input>
      <input type="text" placeholder="Memoria" id="memoria" value={this.state.memoria} onChange={this.handleChange}></input>
      <input type="text" placeholder="Armazenamento" id="armazenamento" value={this.state.armazenamento} onChange={this.handleChange}></input>
      <input type="text" placeholder="Estado" id="estado" value={this.state.estado} onChange={this.handleChange}></input>
      <button type="submit">Adicionar</button>
    </form>
    );
  }

}
