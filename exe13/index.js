const divResultado = document.getElementById("resultado");
var apostadores = [];

function comprar (){
    try{
        const jogador = document.getElementById("nomeCompleto");
        if(!jogador.value){
            throw new Error("É nessário informar o nome completo do jogador!");
        }

        const numeros = [...document.getElementById("numeros").children];
        const jogo = numeros.map((n) => {
            if(!n.value){
                throw new Error("É nessário preencher todos os números para comprar o ticket!");
            }

            return n.value;
        }).join(" - ");
        
        const div = document.createElement("div");
        div.innerText = `Jogador: ${jogador.value} -> ${jogo}`;

        document.getElementById("apostadores").appendChild(div);

        apostadores.push({
            jogador: jogador.value,
            jogo
        });

        jogador.value = "";
        numeros.forEach((n) => n.value = "");
    }catch(error){
        alert(error.message);
    }
}

function gerar(){
    const resultado = `${getNumeroAleatorio()} - ${getNumeroAleatorio()} - ${getNumeroAleatorio()} - ${getNumeroAleatorio()} - ${getNumeroAleatorio()} - ${getNumeroAleatorio()}`;
    divResultado.innerText = `Resultado: ${resultado}`;

    const ganhadores = apostadores.filter((apostador) => apostador.jogo === resultado);
    if(ganhadores.length){
        alert("Ganhador(es): " + ganhadores.map((jogador) => jogador.jogador).join(", "));
    }else{
        alert("Não existem ganhadores!");
    }

    apostadores = [];
    document.getElementById("apostadores").innerHTML = "";
}

function getNumeroAleatorio() {
    return Math.ceil(Math.random() * 60);
}
