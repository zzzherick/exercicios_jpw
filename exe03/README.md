Definição:

Estilize o formulário do exercício anterior com CSS para que fique como o mockup. O CSS deve seguir as seguintes especificações:
	O fundo da página deve ser da cor #8083c9 e a fonte deve monospace
	A cor da fonte deve ser white
	O formulário deve estar posicionado horizontalmente ao centro da página,com tamanho máximo de 20rem;
	A cor de fundo do formulário deve ser branca, com bordas sólidas na corslateblue de 2px;
	Os títulos dos campos devem estar na cor rgb(38, 28, 105), em maiúsculaOs inputs de texto devem ter uma borda abaixo de 1px na cor 		lightgray.Também devem ocupar 100% do espaço disponível no formulário
	O botão de registro deve ser da cor slateblue, com a fonte branca
