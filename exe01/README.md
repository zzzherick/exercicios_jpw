Definição:

Crie uma página em HTML que contenha uma lista contendo jogos, filmes ouséries do seu gosto. 
Esta lista deve conter:
	Uma sinopse;
	Uma seção com imagens;
	Uma lista contendo nome e descrição dos personagens;
	Um link para uma página externa (ex.: Wikipedia);
Outras informações também são bem-vindas.
