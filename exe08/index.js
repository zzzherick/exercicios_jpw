const POKEMONS_POR_LINHA = 4;

const pokemons = [
    {
        numero: 1,
        nome: "Bulbasaur",
        tipo: "Grama",
        descricao: "Ele é verde",
        url: "https://assets.pokemon.com/assets/cms2/img/pokedex/full/001.png"
    },
    {
        numero: 2,
        nome: "Ivysaur",
        tipo: "Grama",
        descricao: "Ele é verde",
        url: "https://assets.pokemon.com/assets/cms2/img/pokedex/full/002.png"
    },
    {
        numero: 3,
        nome: "Venusaur",
        tipo: "Grama",
        descricao: "Ele é verde",
        url: "https://assets.pokemon.com/assets/cms2/img/pokedex/full/003.png"
    },
    {
        numero: 4,
        nome: "Charmander",
        tipo: "Fogo",
        descricao: "Ele é laranja",
        url: "https://assets.pokemon.com/assets/cms2/img/pokedex/full/004.png"
    },
    {
        numero: 5,
        nome: "Charmeleon",
        tipo: "Fogo",
        descricao: "Ele é laranja",
        url: "https://assets.pokemon.com/assets/cms2/img/pokedex/full/005.png"
    },
    {
        numero: 6,
        nome: "Charizard",
        tipo: "Planta",
        descricao: "Ele é laranja",
        url: "https://assets.pokemon.com/assets/cms2/img/pokedex/full/006.png"
    },
    {
        numero: 7,
        nome: "Squirtle",
        tipo: "Água",
        descricao: "Ele é azul",
        url: "https://assets.pokemon.com/assets/cms2/img/pokedex/full/007.png"
    },
    {
        numero: 8,
        nome: "Wartortle",
        tipo: "Água",
        descricao: "Ele é azul",
        url: "https://assets.pokemon.com/assets/cms2/img/pokedex/full/008.png"
    },
    {
        numero: 9,
        nome: "Blastoise",
        tipo: "Água",
        descricao: "Ele é azul",
        url: "https://assets.pokemon.com/assets/cms2/img/pokedex/full/009.png"
    },
    {
        numero: 10,
        nome: "Caterpie",
        tipo: "Inseto",
        descricao: "Ele é verde",
        url: "https://assets.pokemon.com/assets/cms2/img/pokedex/full/010.png"
    },
];

var edicaoAtual = null;
var showcaseIndex = 0;
var showcaseTimeout = null;

function getPokemonFromForm(){
    return {
        numero: document.getElementById("numero").value,
        nome: document.getElementById("nome").value,
        tipo: document.getElementById("tipo").value,
        descricao: document.getElementById("descricao").value,
        url: document.getElementById("url").value
    };
}

function resetForm(){
    document.getElementById("numero").value = "";
    document.getElementById("nome").value = "";
    document.getElementById("tipo").value = "";
    document.getElementById("descricao").value = "";
    document.getElementById("url").value = "";
}

function createItem(id, pokemon){
    let item = document.createElement("div");
    item.classList.add("item");

    let h4 = document.createElement("h4");
    h4.innerText = `#${pokemon.numero} - ${pokemon.nome}`;
    item.appendChild(h4);

    let imageWrapper = document.createElement("div");
    imageWrapper.classList.add("item-img-wrapper");
    item.appendChild(imageWrapper);

    let img = document.createElement("img");
    img.src = pokemon.url;
    img.classList.add("item-img");
    imageWrapper.appendChild(img);

    let spam = document.createElement("spam");
    spam.innerText = `Tipo: ${pokemon.tipo}`;
    item.appendChild(spam);

    let p = document.createElement("p");
    item.appendChild(p);

    let small = document.createElement("small");
    small.innerText = `Descrição: ${pokemon.descricao}`;
    p.appendChild(small);

    let actionWrapper = document.createElement("div");
    actionWrapper.classList.add("item-action-wrapper");
    item.appendChild(actionWrapper);

    let editar = document.createElement("button");
    editar.classList.add("action-edit");
    editar.innerText = "Editar"
    editar.setAttribute("onclick", `updateItem(${id})`);
    actionWrapper.appendChild(editar);

    let excluir = document.createElement("button");
    excluir.classList.add("action-delete");
    excluir.innerText = "Excluir"
    excluir.setAttribute("onclick", `deleteItem(${id})`);
    actionWrapper.appendChild(excluir);

    return item;
}

function createEmptyItem(){
    let emptyItem = document.createElement("div");
    emptyItem.classList.add("empty-item");

    return emptyItem;
}

function updateItem(id){
    let pokemon = pokemons[id];

    document.getElementById("numero").value = pokemon.numero;
    document.getElementById("nome").value = pokemon.nome;
    document.getElementById("tipo").value = pokemon.tipo;
    document.getElementById("descricao").value = pokemon.descricao;
    document.getElementById("url").value = pokemon.url;

    edicaoAtual = id;
    document.getElementById("submit").innerText = "Editar";
}

function deleteItem(id){
    pokemons.splice(id, 1);
    addAll();
}

function addAll(){

    let tamanhoLista = pokemons.length;
    let totalLinhas = Math.ceil(tamanhoLista / POKEMONS_POR_LINHA);

    let lista = document.getElementById("lista");
    lista.innerHTML = "";

    let index = 0;
    for(let i=0; i<totalLinhas; i++){
        
        let linha = document.createElement("div");
        linha.classList.add("linha");
        
        for(let j=0; j<POKEMONS_POR_LINHA; j++, index++){
            linha.appendChild(index < tamanhoLista ? createItem(index, pokemons[index]) : createEmptyItem());
        }

        lista.appendChild(linha);
    }
}

function submit(){
    if(edicaoAtual != null){
        pokemons[edicaoAtual] = getPokemonFromForm()
        document.getElementById("submit").innerText = "Adicionar";
        edicaoAtual = null;
    }else{
        pokemons.push(getPokemonFromForm());
    }

    addAll();

    resetForm();
}

function showcase(){

    if(pokemons.length == 0){
        return;
    }

    let lista = document.getElementById("lista");
    let showcase = document.getElementById("showcase");
    let showcaseBtn = document.getElementById("showcaseBtn");

    if(showcaseTimeout == null){
        lista.style.display = "none";
        showcase.style.display = "inline";
        showcaseBtn.innerText = "Desativar Showcase";

        setPokemonShowcase();
    }else{
        lista.style.display = "inline";
        showcase.style.display = "none"; 
        showcaseBtn.innerText = "Ativar Showcase";

        window.clearTimeout(showcaseTimeout);
        
        showcaseTimeout = null;
        showcaseIndex = 0;
    }
}

function setPokemonShowcase(){

    let pokemon = pokemons[showcaseIndex];

    document.getElementById("showcaseNome").innerText = `Pokémon: ${pokemon.nome}`;
    document.getElementById("showcaseImg").src = pokemon.url;

    showcaseIndex++;
    if(showcaseIndex >= pokemons.length){
        showcaseIndex = 0;
    }

    showcaseTimeout = setTimeout(setPokemonShowcase, 3000);
}

addAll();