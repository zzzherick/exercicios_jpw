import React from 'react';

export default class Modo extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      modos: ["⌇⌇⌇", "❄", "☀"],
      indexModo: 0,
      modo: "⌇⌇⌇"
    };

  }

  alterarModo = () => {
    let novoModo = this.state.indexModo;
    if(novoModo === 2){
      novoModo = 0;
    }else{
      novoModo++;
    }

    this.setState({
      indexModo: novoModo,
      modo: this.state.modos[novoModo]
    });
  }

  render(){
    return(
    <div>
      Modo: {this.state.modo}<br />
      <button onClick={this.alterarModo}>Modo</button>
    </div>
    );
  }

}
