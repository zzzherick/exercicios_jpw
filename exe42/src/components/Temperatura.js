import React from 'react';

export default class Temperatura extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      valor: 22
    };
  }

  menosTemperatura = () => {
    this.setState({
      valor: this.state.valor - 1
    });
  }
  
  maisTemperatura = () => {
    this.setState({
      valor: this.state.valor + 1
    });
  }

  render(){
    return(
    <div>
      Temperatura: {this.state.valor}<br />
      <button onClick={this.menosTemperatura}>-</button>
      <button onClick={this.maisTemperatura}>+</button>
    </div>
    );
  }

}
