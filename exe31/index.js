const express = require("express");
const app = express();
const gerenciaLista = require("./gerencia-lista");

app.use(express.static("public"));

app.get("/produtos/add", (req, res) => {
    gerenciaLista.adicionar(req.query);
    res.redirect("/"); 
});

app.get("/produtos/listar", (req, res) => {
    res.json(gerenciaLista.listar()); 
});

app.listen(3000, () => {
    console.log('Aplicação executando na porta 3000');
});