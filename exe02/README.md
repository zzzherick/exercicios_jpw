Definição:

Crie uma página chamada criacao_personagem.html que contenha umformulário para criação de um personagem, contendo os seguintes dados:
	Nome do Personagem [nome]
	Cidade Natal (Thais, Carlin, Venore) [cidade]
	Classe (Guerreiro, Arqueiro, Mago) [classe]
	Raça (Humano, Elfo, Anão) [raca]
	Realizar Tutorial? (Boolean) [tutorial]
O formulário deve ser do tipo GET e enviar os dados parahttps://venson.net.br/professor/ws/personagem
