function validarForm() {
    const form = document.forms["criacao"];

    try{
        const nome = form["nome"].value;
        if(!nome || nome.length < 3){
            throw "O nome do personagem deve conter pelo menos 3 letras!";
        }
        
        if(nome.startsWith("Admin") || nome.startsWith("GM") || nome.startsWith("Moderador")){
            throw "O nome do personagem não pode iniciar com a palavra informada!";
        }

        const cidade = form["cidade"].value;
        const raca = form["raca"].value;

        if(cidade === "Venore" && raca === "Elfo"){
            throw "Elfos são proibidos Venore!";
        }

        if(cidade === "Carlin" && raca === "Anão"){
            throw "Anões são proibidos Carlin!";
        }

    }catch(e){
        alert(e);
        return false;
    }

    return true;
  } 